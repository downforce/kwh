<?php

require($_SERVER['DOCUMENT_ROOT'].'/includes/common.php');
//$setdebug = true;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once $documentroot.'/includes/header.php' ?>
	<meta name="description" content="pgm's Electricity Estimator">
	<title>kWh :: pgm.id.au</title>

</head>
<body>
	<?php $page = 'kWh'; include_once($documentroot.'/menu.php'); ?>
	<div class="container">
		<div class="jumbotron">
			<h1>Electricity Estimator</h1>
			<!--<p><a class="btn btn-primary btn-large">Learn more &raquo;</a></p>-->
		</div>

		<!-- The cost per unit for Synergy is 23.550200 -->
		<form role="form" name="calculator">
			<input type="hidden" id="num1000" name="num1000" value="1000">
			<input type="hidden" id="numDay" name="numDay" value="0.1428571428571429">
			<input type="hidden" id="numWeek" name="numWeek" value="1">
			<input type="hidden" id="numMonth" name="numMonth" value="4.33">
			<input type="hidden" id="numYear" name="numYear" value="52">
			<div>
				<h4>Inputs:</h4>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="numWatts">Output in Watts</label>
						<input type="number" class="form-control" id="numWatts" name="numWatts" placeholder="Watts (W)">
					</div> <!-- /form-group -->
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="numHrsConsumption">Hours</label>
						<div class="input-group">
							<div class="input-group-btn">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">per day <span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li><a href="#">per day</a></li>
									<li><a href="#">per week</a></li>
									<li><a href="#">per month</a></li>
									<li><a href="#">per year</a></li>
								</ul>
							</div><!-- /btn-group -->
							<input type="number" class="form-control" id="numHrsConsumption" name="numHrsConsumption" value="10">
						</div><!-- /input-group -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="numCostPerUnit">Cost per Unit</label>
						<input type="text" class="form-control" id="numCostPerUnit" name="numCostPerUnit" value="0.23550200" tooltip="Cost is based on Synergy">
					</div>
				</div>
			</div>
			<div>
				<h4>Estimated Cost:</h4>
			</div>
			<div class="row">
				<label class="col-md-2" for="numCostDay">Per Day</label>
				<div class="col-md-4">
					<input type="text" class="form-control" id="numCostDay" name="numCostDay" placeholder="Cost per day" jAutoCalc="{numWatts} / {num1000} * {numHrsConsumption} * {numCostPerUnit} * {numDay}">
				</div>
			</div>
			<div class="row">
				<label class="col-md-2" for="numCostWeek">Per Week</label>
				<div class="col-md-4">
					<input type="text" class="form-control" id="numCostWeek" name="numCostWeek" placeholder="Cost per week" jAutoCalc="{numWatts} / {num1000} * {numHrsConsumption} * {numCostPerUnit} * {numWeek}">
				</div>
			</div>
			<div class="row">
				<label class="col-md-2" for="numCostMonth">Per Month</label>
				<div class="col-md-4">
					<input type="text" class="form-control" id="numCostMonth" name="numCostMonth" placeholder="Cost per month" jAutoCalc="{numWatts} / {num1000} * {numHrsConsumption} * {numCostPerUnit} * {numMonth}">
				</div>
			</div>
			<div class="row">
				<label class="col-md-2" for="numCostYear">Per Year</label>
				<div class="col-md-4">
					<input type="text" class="form-control" id="numCostYear" name="numCostYear" placeholder="Cost per year" jAutoCalc="{numWatts} / {num1000} * {numHrsConsumption} * {numCostPerUnit} * {numYear}">
				</div>
			</div>
			<div class="row" >
			</div>
		</form>

		<hr />
		<footer class="footer">
			<?php include_once($documentroot.'/includes/footer.php'); ?>
			<?php include_once($documentroot.'/includes/scripts.php'); ?>
			<script type="text/javascript" src="kWh.js"></script>
			<!-- ### Page specific scripts ### -->
			<!-- Reference for AutoCalc: https://github.com/sauerc/jAutoCalc-->
			<script type="text/javascript" src="//rawgithub.com/sauerc/jAutoCalc/master/jAutoCalc.js"></script>
			<script type="text/javascript">
				$(document).ready(function() {
					$('form[name=calculator] tr[name=numWatts]').jAutoCalc({keyEventsFire: true, decimalPlaces: 2});
					$('form[name=calculator] tr[name=numHrsConsumption]').jAutoCalc({keyEventsFire: true, decimalPlaces: 2});
					$('form[name=calculator] tr[name=numCostPerUnit]').jAutoCalc({keyEventsFire: true, decimalPlaces: 2});
					$('form[name=calculator]').jAutoCalc({decimalPlaces: 2});

				});
			</script>			
		</footer>
	</div> <!-- /container -->
</body>
</html>